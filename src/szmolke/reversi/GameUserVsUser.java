package szmolke.reversi;

import szmolke.gui.GameBoard;
import szmolke.gui.Pawn;

/**
 * Created by Łukasz on 26.05.2016.
 */
public class GameUserVsUser implements Game {
    private Solve solve;
    private RefreshBoardCallback<Pawn[]> refreshBoardCallback;

    public GameUserVsUser(Solve solve, RefreshBoardCallback<Pawn[]> refreshBoardCallback) {
        this.solve = solve;
        this.refreshBoardCallback = refreshBoardCallback;
        setGameType();
    }

    @Override
    public void onPawnPlaced(Pawn pawn, int row, int col) {
        Logic logic = solve.getLogic();
        Pawn[] pawns = logic.getOrginalPawns();
        if(logic.isCorrectAdd(pawns, row * GameBoard.BOARD_SIZE + col, pawn)) {
            logic.placePawn(pawns, pawn, row, col);
            GameBoard.setOpositeTurn();
            refreshBoardCallback.apply(pawns);
            if(!logic.isAvailableToAdd(pawns, GameBoard.getTurn())) {
                GameBoard.setOpositeTurn();
                System.out.println("brak ruchu");
            }
        }
    }

    @Override
    public void setGameType() {
        GameBoard.gameType = GameType.UserVsUser;
    }

    @FunctionalInterface
    public interface RefreshBoardCallback<One> {
        void apply(One one);
    }
}
