package szmolke.reversi;

import szmolke.gui.GameBoard;
import szmolke.gui.Pawn;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Łukasz on 19.05.2016.
 */
public class Logic {
    private AddPawn<Pawn, Integer, Integer> addPawn;
    private RemovePawn<Integer, Integer> removePawn;
    private Pawn[] pawns;

    public Logic(Pawn[] pawns, AddPawn<Pawn, Integer, Integer> addPawn, RemovePawn<Integer, Integer> removePawn) {
        this.addPawn = addPawn;
        this.removePawn = removePawn;
        this.pawns = pawns;

//        Pawn[] pawns1 = new Pawn[pawns.length];
//        for (int i = 0; i <= 56; i++) {
//            pawns1[i] = Pawn.BLACK;
//        }
//        pawns1[57] = Pawn.WHITE;
//        GameBoard.printBoard(pawns1);
//        System.out.println(isAvailableToAdd(pawns1, Pawn.WHITE));
    }

    public void placePawn(Pawn[] pawns, Pawn pawn, int row, int col) {
        setPawn(pawns, pawn, row, col);
    }

    public boolean isAvailableToAdd(Pawn[] pawns, Pawn playerPawn) {
        boolean isAvailable = false;
        for (int index = 0; index < pawns.length && !isAvailable; index++) {
            for (int actual = 0; actual < pawns.length && !isAvailable; actual++) {
                if (index != actual && pawns[index] == null && pawns[actual] != null && playerPawn.equals(pawns[actual])
                        && (isInRow(index, actual) || isInCol(index, actual) || isInDiagonal(index, actual)) && isOponentBetween(pawns, index, actual)) {
                    isAvailable = true;
                }
            }
        }
        return isAvailable;
    }

    public List<Integer> getAvailableIndexes(Pawn[] pawns, Pawn playerPawn) {
        List<Integer> list = new LinkedList<>();
        for (int index = 0; index < pawns.length; index++) {
            for (int actual = 0; actual < pawns.length; actual++) {
                if (index != actual && pawns[index] == null && pawns[actual] != null && playerPawn.equals(pawns[actual])
                        && (isInRow(index, actual) || isInCol(index, actual) || isInDiagonal(index, actual)) && isOponentBetween(pawns, index, actual)) {
                        list.add(index);
                }
            }
        }
        return list;
    }

    public int getAvailablePlacesCount(Pawn[] pawns, Pawn playerPawn) {
        int count = 0;
        for (int index = 0; index < pawns.length; index++) {
            for (int actual = 0; actual < pawns.length; actual++) {
                if (index != actual && pawns[index] == null && pawns[actual] != null && playerPawn.equals(pawns[actual])
                        && (isInRow(index, actual) || isInCol(index, actual) || isInDiagonal(index, actual)) && isOponentBetween(pawns, index, actual)) {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean isCorrectAdd(Pawn[] pawns, int index, Pawn playerPawn) {
        boolean isCorrect = false;
        if(pawns[index] == null) {
            for (int i = 0; i < pawns.length; i++) {
                if (pawns[i] != null && pawns[i].equals(playerPawn) && (isInRow(index, i) || isInCol(index, i) || isInDiagonal(index, i))) {
                    boolean correct = isOponentBetween(pawns, index, i);
                    if(correct) {
                        changePawnsToOposite(pawns, playerPawn, index, i);
                        isCorrect = true;
                    }
                }
            }
        }
        return isCorrect;
    }

    public void changePawnsToOposite(Pawn[] pawns, Pawn pawn, int index, int actual) {
        int counter = getCounter(index, actual);
        while(actual != index) {
            index += counter;
            pawns[index] = pawn;
        }
    }

    private boolean isOponentBetween(Pawn[] pawns, int index, int actual) {
        boolean isOponentBetween = false;
        int counter = getCounter(index, actual);

        while(actual != index) {
            index += counter;
            if(pawns[index] != null && !pawns[actual].equals(pawns[index])) {
                isOponentBetween = true;
            }
            if(actual != index && (pawns[index] == null || pawns[actual].equals(pawns[index]))) {
                isOponentBetween = false;
                break;
            }
        }
        return isOponentBetween;
    }

    private int getCounter(int index, int actual) {
        int difference = index - actual;
        int couter = 0;

        if(isInRow(index, actual)) {
            couter = 1;
        } else if(isInCol(index, actual)) {
            couter = GameBoard.BOARD_SIZE;
        } else {
            if((Math.abs(difference) % (GameBoard.BOARD_SIZE + 1)) == 0) {
                couter = GameBoard.BOARD_SIZE + 1;
            } else {
                couter = GameBoard.BOARD_SIZE - 1;
            }
        }
        if(difference > 0) {
            couter *= -1;
        }
        return couter;
    }

    public Pawn[] getOrginalPawns() {
        return pawns;
    }

    private boolean isInDiagonal(int index, int actual) {
        return (Math.abs(index - actual) % (GameBoard.BOARD_SIZE - 1)) == 0 || (Math.abs(index - actual) % (GameBoard.BOARD_SIZE + 1)) == 0;
    }

    private boolean isInCol(int index, int actual) {
        return (index % GameBoard.BOARD_SIZE) == (actual % GameBoard.BOARD_SIZE);
    }

    private boolean isInRow(int index, int actual) {
        return (index / GameBoard.BOARD_SIZE) == (actual / GameBoard.BOARD_SIZE);
    }

    public void setPawn(Pawn[] pawns, Pawn pawn, int row, int col) {
        pawns[row*GameBoard.BOARD_SIZE + col] = pawn;
    }

    public int getRow(int index) {
        return index / GameBoard.BOARD_SIZE;
    }

    public int getCol(int index) {
        return index % GameBoard.BOARD_SIZE;
    }

    @FunctionalInterface
    public interface RemovePawn<One, Two> {
        void apply(One one, Two two);
    }

    @FunctionalInterface
    public interface AddPawn<One, Two, Three> {
        void apply(One one, Two two, Three three);
    }
}
