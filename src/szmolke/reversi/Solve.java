package szmolke.reversi;


import javafx.application.Platform;
import szmolke.gui.GameBoard;
import szmolke.gui.Pawn;

/**
 * Created by Łukasz on 20.05.2016.
 */
public class Solve {
    private Logic logic;
    private Pawn playerPawn;
    private EvaluationHeuristic evaluation;
    private SearchTreeHeuristic searchTreeHeu;
    private BasicAlgorithm basicAlgorithm;
    private RefreshBoardCallback<Pawn[]> refreshBoardCallback;
    private int maxDepth;

    public Solve(Logic logic, Pawn playerPawn) {
        this.logic = logic;
        this.playerPawn = playerPawn;
    }

    public Solve(Logic logic, Pawn playerPawn,BasicAlgorithm basicAlgorithm ,EvaluationHeuristic evaluation, SearchTreeHeuristic searchTreeHeu, int maxDepth) {
        this(logic, playerPawn);
        this.basicAlgorithm = basicAlgorithm;
        this.evaluation = evaluation;
        this.searchTreeHeu = searchTreeHeu;
        this.maxDepth = maxDepth;
    }

    public void setRefreshBoardCallback(RefreshBoardCallback<Pawn[]> refreshCallBack) {
        this.refreshBoardCallback = refreshCallBack;
    }

    public synchronized void nextMove(Pawn pawn) {
        Pawn[] actualPawns = logic.getOrginalPawns();
        int result = getBestIndex(actualPawns, pawn);
        //System.out.println("result: "+result);
        if(result == -1) {
            System.out.println("Brak ruchu");
            GameBoard.setOpositeTurn();
        } else {
            logic.isCorrectAdd(actualPawns, result, GameBoard.getTurn());
            logic.placePawn(actualPawns, GameBoard.getTurn(), logic.getRow(result), logic.getCol(result));
            Platform.runLater(() -> refreshBoardCallback.apply(actualPawns));
//            logic.getAvailableIndexes(actualPawns, GameBoard.getTurn().getOposite())
//            .stream().forEach(integer -> System.out.print(integer+", "));
//            System.out.println();
        }
    }

    public int getBestIndex(Pawn[] pawns, Pawn pawn) {
        int evaluation;
        Pawn[] childBoard = clonePawns(pawns);
        Pawn actual = pawn;
        int maxEvaluation = Integer.MIN_VALUE;
        int result = -1;
        int index = nextIndexHeuristic(pawns, pawn, -1);

        while (index > -1 && index < GameBoard.BOARD_SIZE * GameBoard.BOARD_SIZE) {
            if (logic.isCorrectAdd(childBoard, index, actual)) {
                logic.placePawn(childBoard, actual, logic.getRow(index), logic.getCol(index));
                evaluation = getBasicAlgorithm(childBoard, actual, 0);
                childBoard = clonePawns(pawns);
                if (evaluation > maxEvaluation) {
                    result = index;
                    maxEvaluation = evaluation;
                }
            }
            index = nextIndexHeuristic(pawns, pawn, index);
        }
//        lastPlacedIndexes.add(result);
//        if (lastPlacedIndexes.size() > 6) {
//            lastPlacedIndexes.remove(0);
//        }
        return result;
    }

    private int minMaxAlgorithm(Pawn[] pawns, Pawn playerPawn, int depth) {
        if(depth == maxDepth) {
            return evaluationFunction(pawns, playerPawn);
        }
        Pawn nextPlayer = playerPawn.getOposite();
        int evaluation;

        if(depth % 2 != 0) {
            int bestValue = Integer.MAX_VALUE;
            int index = nextIndexHeuristic(pawns, playerPawn, -1);
            while(index < GameBoard.BOARD_SIZE*GameBoard.BOARD_SIZE) {
                if(logic.isCorrectAdd(pawns, index, nextPlayer)) {
                    logic.placePawn(pawns, nextPlayer, logic.getRow(index), logic.getCol(index));
                    pawns = clonePawns(pawns);
                    evaluation = minMaxAlgorithm(pawns, nextPlayer, depth + 1);
                    bestValue = Math.min(bestValue, evaluation);
                    return bestValue;
                }
                index = nextIndexHeuristic(pawns, nextPlayer, index);
            }
        } else {
            int bestValue = Integer.MIN_VALUE;
            int index = nextIndexHeuristic(pawns, playerPawn, -1);
            while(index < GameBoard.BOARD_SIZE*GameBoard.BOARD_SIZE) {
                if (logic.isCorrectAdd(pawns, index, nextPlayer)) {
                    logic.placePawn(pawns, nextPlayer, logic.getRow(index), logic.getCol(index));
                    pawns = clonePawns(pawns);
                    evaluation = minMaxAlgorithm(pawns, nextPlayer, depth + 1);
                    bestValue = Math.max(bestValue, evaluation);
                    return bestValue;
                }
                index = nextIndexHeuristic(pawns, nextPlayer, index);
            }
        }
        return -1;
    }

    private int alfaBeta(Pawn[] pawns, Pawn playerPawn, int depth, int alfa, int beta) {
        if(depth == maxDepth) {
            return evaluationFunction(pawns, playerPawn);
        }
        Pawn nextPlayer = playerPawn.getOposite();
        int evaluation;

        if(depth % 2 != 0) {
            int index = nextIndexHeuristic(pawns, playerPawn, -1);
            while(index < GameBoard.BOARD_SIZE*GameBoard.BOARD_SIZE) {
                if(logic.isCorrectAdd(pawns, index, nextPlayer)) {
                    logic.placePawn(pawns, nextPlayer, logic.getRow(index), logic.getCol(index));
                    pawns = clonePawns(pawns);
                    evaluation = alfaBeta(pawns, nextPlayer, depth + 1, alfa, beta);
                    //System.out.println(index+" min eval: "+evaluation);
                    beta = Math.min(beta, evaluation);
                    if(alfa >= beta) {
                        return beta;
                    }
                }
                index = nextIndexHeuristic(pawns, nextPlayer, index);
            }
            return beta;
        } else {
            int index = nextIndexHeuristic(pawns, playerPawn, -1);
            while(index < GameBoard.BOARD_SIZE*GameBoard.BOARD_SIZE) {
                if (logic.isCorrectAdd(pawns, index, nextPlayer)) {
                    logic.placePawn(pawns, nextPlayer, logic.getRow(index), logic.getCol(index));
                    pawns = clonePawns(pawns);
                    evaluation = alfaBeta(pawns, nextPlayer, depth + 1, alfa, beta);
                    //System.out.println(index+" max eval: "+evaluation);
                    alfa = Math.max(alfa, evaluation);
                    if(alfa >= beta) {
                        return alfa;
                    }
                }
                index = nextIndexHeuristic(pawns, nextPlayer, index);
            }
            return alfa;
        }
    }

    private int getBasicAlgorithm(Pawn[] pawns, Pawn playerPawn, int startDepth) {
        int evaluation = 0;
        switch (basicAlgorithm) {
            case MIN_MAX:
                evaluation = minMaxAlgorithm(pawns, playerPawn, startDepth);
                break;
            case ALPHA_BETA:
                evaluation = alfaBeta(pawns, playerPawn, startDepth, Integer.MIN_VALUE, Integer.MAX_VALUE);
                break;
        }
        return evaluation;
    }

    private int evaluationFunction(Pawn[] pawns, Pawn playerPawn) {
        int evaluate = 0;
        switch (evaluation) {
            case MOST_PAWNS:
                evaluate = PointsToSolver.getPointForPawns(pawns, playerPawn);
                break;
            case BEST_PLACES:
                evaluate = PointsToSolver.getPointForPlace(pawns, playerPawn);
                break;
            case OPPONENT_MOVES:
                evaluate = PointsToSolver.getPointForOpponentMoves(logic.getAvailablePlacesCount(pawns, playerPawn));
                break;
        }
        return evaluate;
    }

    private int nextIndexHeuristic(Pawn[] pawns, Pawn playerPawn, int actualIndex) {
        switch (searchTreeHeu) {
            case INCREMENTS:
                actualIndex++;
                break;
        }
        return actualIndex;
    }

    private Pawn[] clonePawns(Pawn[] pawns) {
        Pawn[] pawns1 = new Pawn[pawns.length];
        for (int i = 0; i < pawns.length; i++) {
            pawns1[i] = pawns[i];
        }
        return pawns1;
    }

    public Logic getLogic() {
        return logic;
    }

    @FunctionalInterface
    public interface RefreshBoardCallback<One> {
        void apply(One one);
    }

    public enum EvaluationHeuristic {
        MOST_PAWNS, BEST_PLACES, OPPONENT_MOVES
    }

    public enum SearchTreeHeuristic {
        INCREMENTS
    }

    public enum BasicAlgorithm {
        MIN_MAX, ALPHA_BETA
    }
}
