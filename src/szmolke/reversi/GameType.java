package szmolke.reversi;

/**
 * Created by Łukasz on 26.05.2016.
 */
public enum GameType {
    UserVsUser, UserVsPc, PcVsPc;
}
