package szmolke.reversi;

import szmolke.gui.GameBoard;
import szmolke.gui.Pawn;

/**
 * Created by Łukasz on 27.05.2016.
 */
public class PointsToSolver {
    //punkty za położenie
    private static final int CORNER_POINT = 25;
    private static final int EDGE_POINT = 20;
    private static final int NEUTRAL_POINT = 15;
    private static final int NEAR_CORNER_POINT = 10;
    private static final int NEAR_EDGE_POINT = 5;
    private static final int NEAR_CORNER_DIAG_POINT = 1;

    //punkty za brak ruchu przeciwnika
    private static final int OPPONENT_CANT_MOVE = 15;
    private static final int OPPONENT_CAN_MOVE = -30;

    public static int getPointForPawns(Pawn[] pawns, Pawn playerPawn) {
        int count = 0;
        for (int i = 0; i < pawns.length; i++) {
            if(playerPawn.equals(pawns[i])) {
                count++;
            }
        }
        return count;
    }

    public static int getPointForPlace(Pawn[] pawns, Pawn playerPawn) {
        int points = 0;
        for (int i = 0; i < pawns.length; i++) {
            if(playerPawn.equals(pawns[i])) {
                points += getPoints(i);
            }
        }
        return points;
    }

    public static int getPointForOpponentMoves(int opponentMoves) {
        int points = OPPONENT_CAN_MOVE * opponentMoves;
        points += OPPONENT_CANT_MOVE * (GameBoard.BOARD_SIZE*GameBoard.BOARD_SIZE - 4 - opponentMoves);
        return points;
    }

    private static int getPoints(int index) {
        if(index == 0 || index == 7 || index == 56 || index == 63) {
            return CORNER_POINT;
        } else if ((index >= 2 && index <= 5) || (index >= 58 && index <= 61) ||
                (index >= 16 && index <= 47 && (isRestFromDevideEqual(index,0,0) || isRestFromDevideEqual(index, GameBoard.BOARD_SIZE - 1, GameBoard.BOARD_SIZE - 1)))) {
            return EDGE_POINT;
        } else if (index >= 18 && index <= 45 && isRestFromDevideEqual(index, 2, 5)) {
            return NEUTRAL_POINT;
        } else if (index == 1 || index == 6 || index == 8 || index == 15 || index == 48 || index == 55 || index == 57 || index == 62){
            return NEAR_CORNER_POINT;
        } else if ((((index >= 10 && index <= 13)||(index >= 50 && index <= 53)) && isRestFromDevideEqual(index, 2, 5))
                    || ((index >= 17 && index <= 46) && (isRestFromDevideEqual(index, 1, 1) || isRestFromDevideEqual(index, 6, 6)))) {
            return NEAR_EDGE_POINT;
        } else {
            return NEAR_CORNER_DIAG_POINT;
        }
    }

    private static boolean isRestFromDevideEqual(int index, int from, int to) {
        return index % GameBoard.BOARD_SIZE >= from && index % GameBoard.BOARD_SIZE <= to;
    }
}
