package szmolke.reversi;

import szmolke.gui.GameBoard;
import szmolke.gui.Pawn;

/**
 * Created by Łukasz on 26.05.2016.
 */
public class GamePcVsPc implements Game {
    private boolean stop = false;
    private Solve solvePc1, solvePc2;

    public GamePcVsPc(Solve solvePc1, Solve solvePc2) {
        this.solvePc1 = solvePc1;
        this.solvePc2 = solvePc2;
        setGameType();
    }

    @Override
    public synchronized void onPawnPlaced(Pawn pawn, int row, int col) {
        if(!stop) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(GameBoard.getTurn() == Pawn.BLACK) {
                Thread thread = new Thread(() -> solvePc1.nextMove(GameBoard.getTurn()));
                thread.start();
            } else {
                Thread thread = new Thread(() -> solvePc2.nextMove(GameBoard.getTurn()));
                thread.start();
            }
        }
    }

    @Override
    public void setGameType() {
        GameBoard.gameType = GameType.PcVsPc;
    }
}
