package szmolke.reversi;

import szmolke.gui.Pawn;

/**
 * Created by Łukasz on 26.05.2016.
 */
public interface Game {
    void onPawnPlaced(Pawn pawn, int row, int col);
    void setGameType();
}
