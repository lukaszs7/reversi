package szmolke.reversi;

import szmolke.gui.GameBoard;
import szmolke.gui.Pawn;

/**
 * Created by Łukasz on 26.05.2016.
 */
public class GameUserVsPc implements Game {
    private boolean stop = false;
    private Solve solve;

    public GameUserVsPc(Solve solve) {
        this.solve = solve;
        setGameType();
    }

    @Override
    public void onPawnPlaced(Pawn pawn, int row, int col) {
        Logic logic = solve.getLogic();
        if(!stop) {
            Pawn[] pawns = logic.getOrginalPawns();
            if(logic.isCorrectAdd(pawns, row * GameBoard.BOARD_SIZE + col, pawn)) {
                logic.placePawn(pawns, pawn, row, col);
                GameBoard.setOpositeTurn();
                Thread thread = new Thread(() -> solve.nextMove(GameBoard.getTurn()));
                thread.start();
            }
        }
    }

    @Override
    public void setGameType() {
        GameBoard.gameType = GameType.UserVsPc;
    }
}
