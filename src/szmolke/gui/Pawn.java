package szmolke.gui;

/**
 * Created by Łukasz on 19.05.2016.
 */
public enum Pawn {
    WHITE, BLACK;
    private int id;

    public Pawn getOposite() {
        return this == WHITE ? BLACK : WHITE;
    }

    @Override
    public String toString() {
        return this == WHITE ? "Biały" : "Czarny";
    }


}
