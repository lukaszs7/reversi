package szmolke.gui;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import szmolke.reversi.*;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button buttonStartGame, buttonResetGame, buttonCountPoints, buttonStartCompetition, buttonPrintWins;
    public RadioButton radioUserVsUserButton, radioPCvsUserButton, radioPCvsPCButton, radioFirstMinMax, radioFirstAlphaBeta, pc1RadioEvaluatePawns,
            pc1RadioSearchNext, radioSecondMixMax, radioSecondAlphaBeta, pc2RadioSearchNext, pc1RadioEvaluateBestPlace,
            pc1RadioEvaluateOpponentMoves, pc2RadioEvaluatePawns, pc2RadioEvaluateBestPlace, pc2RadioEvaluateOpponentMoves;
    public Text depthPC1Text, depthPC2Text;
    public Slider sliderDepthPC1, sliderDepthPC2;
    private GameBoard gameBoard;
    private Logic logic;
    public StackPane boardPane;
    private int maxDepthPc1, maxDepthPc2;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initComponents();
        setText(depthPC1Text, sliderDepthPC1);
        setText(depthPC2Text, sliderDepthPC2);
        maxDepthPc1 = (int)sliderDepthPC1.getValue();
        maxDepthPc2 = (int)sliderDepthPC2.getValue();
    }

    private void initComponents() {
        buttonStartGame.setOnMouseClicked(event -> {
            initBoard();
            initGameType();
        });
        buttonResetGame.setOnMouseClicked(event -> {
            resetBoard();
        });
        sliderDepthPC1.valueProperty().addListener((observable, oldValue, newValue) -> {
            setText(depthPC1Text, sliderDepthPC1);
            maxDepthPc1 = (int)sliderDepthPC1.getValue();
        });
        sliderDepthPC2.valueProperty().addListener((observable, oldValue, newValue) -> {
            setText(depthPC2Text, sliderDepthPC2);
            maxDepthPc2 = (int)sliderDepthPC2.getValue();
        });
    }

    private void resetBoard() {
        boardPane.getChildren().clear();
        initBoard();
    }

    private void initGameType() {
        if(radioUserVsUserButton.isSelected()) {
            startUserVsUser();
        } else if (radioPCvsUserButton.isSelected()) {
            startUserVsPC(getBasicAlgorithmPC1(), getEvaluationTypePC1(), getSearchHeuristicPC2());
        } else {
            startPcVsPC(getBasicAlgorithmPC1(), getEvaluationTypePC1(), getSearchHeuristicPC1(), getBasicAlgorithmPC2() ,getEvaluationTypePC2(), getSearchHeuristicPC2());
        }
    }

    private void initBoard() {
        gameBoard = new GameBoard(GameBoard.BOARD_SIZE);
        boardPane.getChildren().add(gameBoard.getBoard());
        Pawn[] pawns = gameBoard.getInitPawns();
        logic = new Logic(pawns, gameBoard::putPawn, gameBoard::removePawn);
    }

    private void startUserVsUser() {
        Solve solve = new Solve(logic, GameBoard.START_PAWN);
        Game game = new GameUserVsUser(solve, gameBoard::refreshBoard);
        gameBoard.setAddCallback(game::onPawnPlaced);
        gameBoard.setMouseClickEnabled(true);
    }

    private void startUserVsPC(Solve.BasicAlgorithm basicAlg, Solve.EvaluationHeuristic evaluationHeuristic, Solve.SearchTreeHeuristic searchTreeHeuristic) {
        Solve solve = new Solve(logic, GameBoard.START_PAWN, basicAlg, evaluationHeuristic, searchTreeHeuristic, maxDepthPc1);
        Game game = new GameUserVsPc(solve);
        gameBoard.setAddCallback(game::onPawnPlaced);
        solve.setRefreshBoardCallback(gameBoard::refreshBoard);
        gameBoard.setMouseClickEnabled(true);
    }

    private void startPcVsPC(Solve.BasicAlgorithm basicAlg1, Solve.EvaluationHeuristic pc1EvaluationHeuristic, Solve.SearchTreeHeuristic pc1SearchTreeHeuristic,
                             Solve.BasicAlgorithm basicAlg2, Solve.EvaluationHeuristic pc2EvaluationHeuristic, Solve.SearchTreeHeuristic pc2SearchTreeHeuristic) {
        Solve solvePc1 = new Solve(logic, GameBoard.START_PAWN, basicAlg1,pc1EvaluationHeuristic, pc1SearchTreeHeuristic, maxDepthPc1);
        Solve solvePc2 = new Solve(logic, GameBoard.START_PAWN, basicAlg2,pc2EvaluationHeuristic, pc2SearchTreeHeuristic, maxDepthPc2);

        Game game = new GamePcVsPc(solvePc1, solvePc2);
        gameBoard.setAddCallback(game::onPawnPlaced);
        solvePc1.setRefreshBoardCallback(gameBoard::refreshBoard);
        solvePc2.setRefreshBoardCallback(gameBoard::refreshBoard);
        gameBoard.setMouseClickEnabled(false);
        solvePc1.nextMove(GameBoard.getTurn());
    }

    private void setText(Text text, Slider slider) {
        text.setText(String.valueOf((int)slider.getValue()));
    }

    private Solve.BasicAlgorithm getBasicAlgorithmPC1() {
        if(radioFirstMinMax.isSelected()) {
            return Solve.BasicAlgorithm.MIN_MAX;
        } else if (radioFirstAlphaBeta.isSelected()) {
            return Solve.BasicAlgorithm.ALPHA_BETA;
        } else {
            return null;
        }
    }

    private Solve.BasicAlgorithm getBasicAlgorithmPC2() {
        if(radioSecondMixMax.isSelected()) {
            return Solve.BasicAlgorithm.MIN_MAX;
        } else if (radioSecondAlphaBeta.isSelected()) {
            return Solve.BasicAlgorithm.ALPHA_BETA;
        } else {
            return null;
        }
    }

    private Solve.EvaluationHeuristic getEvaluationTypePC1() {
        if(pc1RadioEvaluatePawns.isSelected()) {
            return Solve.EvaluationHeuristic.MOST_PAWNS;
        } else if (pc1RadioEvaluateBestPlace.isSelected()){
            return Solve.EvaluationHeuristic.BEST_PLACES;
        } else if (pc1RadioEvaluateOpponentMoves.isSelected()) {
            return Solve.EvaluationHeuristic.OPPONENT_MOVES;
        } else {
            return null;
        }
    }

    private Solve.EvaluationHeuristic getEvaluationTypePC2() {
        if(pc2RadioEvaluatePawns.isSelected()) {
            return Solve.EvaluationHeuristic.MOST_PAWNS;
        } else if (pc2RadioEvaluateBestPlace.isSelected()){
            return Solve.EvaluationHeuristic.BEST_PLACES;
        } else if (pc2RadioEvaluateOpponentMoves.isSelected()) {
            return Solve.EvaluationHeuristic.OPPONENT_MOVES;
        } else {
            return null;
        }
    }
    private Solve.SearchTreeHeuristic getSearchHeuristicPC1() {
        if(pc1RadioSearchNext.isSelected()) {
            return Solve.SearchTreeHeuristic.INCREMENTS;
        } else {
            return null;
        }
    }
    private Solve.SearchTreeHeuristic getSearchHeuristicPC2() {
        if(pc2RadioSearchNext.isSelected()) {
            return Solve.SearchTreeHeuristic.INCREMENTS;
        } else {
            return null;
        }
    }


//    public long test2() {
//        long time1 = System.nanoTime();
//        int[] tab = new int[10000];
//        for (int i = 0; i < tab.length; i++) {
//            tab[i] = i;
//        }
//        long time2 = System.nanoTime();
//        return time2 - time1;
//    }
//    public long test() {
//        long time1 = System.nanoTime();
//        int[] tab = new int[10000];
//        Arrays.asList(tab).stream().forEach(elem -> elem[2] = 3);
//        long time2 = System.nanoTime();
//        return time2 - time1;
//    }
}
