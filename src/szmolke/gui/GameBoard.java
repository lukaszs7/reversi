package szmolke.gui;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import szmolke.reversi.Game;
import szmolke.reversi.GameType;

import javax.swing.plaf.basic.BasicTabbedPaneUI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;

/**
 * Created by Łukasz on 19.05.2016.
 */
public class GameBoard {
    public static final int BOARD_RECT_SIZE = 70;
    public static final int BOARD_SIZE = 8;
    public static final Pawn START_PAWN = Pawn.BLACK;
    public static GameType gameType;

    private int size;
    private StackPane board = new StackPane();;
    private Pane pawns = new Pane();
    private Pane rects = new Pane();
    private AddCallback<Pawn, Integer, Integer> addCallback;
    private boolean mouseClickEnabled = true;
    private static Pawn turn = Pawn.BLACK;


    public GameBoard(int size) {
        this.size = size;
        List<Node> rectangles = fillWithRectangles();
        fillInitPawns();
        rects.getChildren().addAll(rectangles);
        board.getChildren().addAll(rects, pawns);
        board.setOnMouseClicked(getMouseListener());
    }

    public Pane getBoard() {
        return board;
    }

    public void setMouseClickEnabled(boolean mouseClickEnabled) {
        this.mouseClickEnabled = mouseClickEnabled;
    }

    public void setAddCallback(AddCallback<Pawn, Integer, Integer> addCallback) {
        this.addCallback = addCallback;
    }

    public List<Node> fillWithRectangles() {
        List<Node> nodes = new ArrayList<>(size*size);
        Rectangle rectangle;
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                rectangle = new Rectangle(j*BOARD_RECT_SIZE, i*BOARD_RECT_SIZE, BOARD_RECT_SIZE, BOARD_RECT_SIZE);
                rectangle.getStyleClass().add("rectangleStyle");
                nodes.add(rectangle);
            }
        }
        return nodes;
    }

    public Pawn[] getInitPawns() {
        Pawn[] newPawns = new Pawn[BOARD_SIZE*BOARD_SIZE];
        List<Node> listPawns = pawns.getChildren();
        String actual = "";
        for (int i = 0; i < listPawns.size(); i++) {
            actual = listPawns.get(i).getStyleClass().toString();
            newPawns[Integer.parseInt(listPawns.get(i).getId())] = actual.equals("circleBlack") ? Pawn.BLACK : Pawn.WHITE;
        }
        return newPawns;
    }

    public static Pawn getTurn() {
        return turn;
    }

    public static void setOpositeTurn() {
        GameBoard.turn = getTurn().equals(Pawn.WHITE) ? Pawn.BLACK : Pawn.WHITE;
    }

    private void fillInitPawns() {
        putPawn(Pawn.BLACK, 3, 3);
        putPawn(Pawn.WHITE, 3, 4);
        putPawn(Pawn.WHITE, 4, 3);
        putPawn(Pawn.BLACK, 4, 4);
    }

    public EventHandler<MouseEvent> getMouseListener() {
        return event -> {
            if(mouseClickEnabled) {
                int row = (int) (event.getY() / BOARD_RECT_SIZE);
                int col = (int) (event.getX() / BOARD_RECT_SIZE);
                if(gameType.equals(gameType.UserVsUser)) {
                    if(event.getButton() == MouseButton.PRIMARY) {
                        addCallback.apply(getTurn(), row, col);
                    } else if (event.getButton() == MouseButton.MIDDLE){
                        removePawn(row, col);
                    }
                } else if (gameType.equals(gameType.UserVsPc)){
                    if(event.getButton() == MouseButton.PRIMARY) {
                        addCallback.apply(Pawn.BLACK, row, col);
                    }
                }
            }
        };
    }

    public void refreshBoard(Pawn[] newPawns) {
        pawns.getChildren().clear();
        for (int i = 0; i < newPawns.length; i++) {
            if(newPawns[i] != null) {
                putPawn(newPawns[i], i / BOARD_SIZE, i % BOARD_SIZE);
            }
        }
        if(gameType != GameType.UserVsUser) {
            setOpositeTurn();
        }

        if(gameType == GameType.PcVsPc) {
            addCallback.apply(getTurn(), 0, 0);
        }
//        pawns.getChildren()
//                .stream()
//                .forEach(elem ->
//                   elem.getStyleClass().set(0, newPawns[Integer.parseInt(elem.getId())].equals(Pawn.BLACK) ? "circleBlack" : "circleWhite")
//                );
    }

    public static void printBoard(Pawn[] pawns) {
        for (int i = 0; i < pawns.length; i++) {
            if(i % BOARD_SIZE == 0 && i != 0) {
                System.out.println();
            }
            Pawn pawn = pawns[i];
            if(pawn == null) {
                System.out.print(0+",");
            } else if (pawn.equals(Pawn.WHITE)){
                System.out.print(1+",");
            } else {
                System.out.print(2+",");
            }
        }
    }

    public void putPawn(Pawn pawnEnum, int row, int col) {
        Circle circle = new Circle(BOARD_RECT_SIZE*(col+1) - BOARD_RECT_SIZE/2, BOARD_RECT_SIZE*(row+1) - BOARD_RECT_SIZE/2, BOARD_RECT_SIZE / 2);
        circle.getStyleClass().add(pawnEnum.equals(Pawn.WHITE) ? "circleWhite" : "circleBlack");
        circle.setId(String.valueOf(row*BOARD_SIZE + col));
        boolean isInList = this.pawns.getChildren().stream().anyMatch(pawn -> pawn.getId().equals(circle.getId()));
        if(!isInList) {
            this.pawns.getChildren().add(circle);
        }
    }

    public void removePawn(int row, int col) {
        pawns.getChildren().removeIf(pawn -> pawn.getId().equals(String.valueOf(row*BOARD_SIZE + col)));
    }

    @FunctionalInterface
    public interface AddCallback<One, Two, Three> {
        void apply(One one, Two two, Three three);
    }
}
